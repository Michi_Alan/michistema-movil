unit FrmInv;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.Controls.Presentation,
  FMX.ScrollBox, Unit1, FMX.Objects;

type
  TFrmInv = class(TFrame)
    GridInv: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    Text1: TText;
    procedure iniciogrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}


procedure TFrmInv.iniciogrid;
var i: integer;
begin
   DataModule1.UniQuery3.Close;
   DataModule1.UniQuery3.SQL.Text := 'select a.direccion, p.nombre, p.precio_unico, i.localizacion_alm, i.existencia from inventario i join almacenes a on a.id = i.id_almacen join unidades_medidas um on um.id = i.id_medida join productos p on p.id = i.id_producto';
   DataModule1.UniQuery3.Open;

   if DataModule1.UniQuery3.RecordCount >0 then
    begin

      for I := 0 to datamodule1.UniQuery3.RecordCount - 1 do
        begin
          GridInv.cells[0, I] := datamodule1.UniQuery3.FieldByName('direccion').AsString;
          GridInv.cells[1, I] := datamodule1.UniQuery3.FieldByName('nombre').AsString;
          GridInv.cells[2, I] := datamodule1.UniQuery3.FieldByName('Precio_unico').AsString;
          GridInv.cells[3, I] := datamodule1.UniQuery3.FieldByName('localizacion_alm').AsString;
          GridInv.cells[4, I] := datamodule1.UniQuery3.FieldByName('existencia').AsString;

          datamodule1.UniQuery3.Next;
        end;

    end;

end;



end.
