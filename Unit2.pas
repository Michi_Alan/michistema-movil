unit Unit2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.ScrollBox, FMX.Objects,
  FMX.MultiView, FMX.Controls.Presentation, FMX.Layouts, System.ImageList,
  FMX.ImgList, FMX.ListBox;

type
  TFrame2 = class(TFrame)
    Layout1: TLayout;
    StringGrid1: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    Text1: TText;
    procedure inicio;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses Unit1;

var
I: integer;
producto: string;

{$R *.fmx}

procedure tframe2.inicio;
begin

  Datamodule1.uniquery2.close;
    Datamodule1.uniquery2.SQL.Text := 'select p.nombre, p.descuento, p.precio_unico, precio_mayoreo from productos p';

    Datamodule1.Uniquery2.Open;

    if Datamodule1.Uniquery2.RecordCount >0 then
    begin

      for I := 0 to Datamodule1.Uniquery2.RecordCount - 1 do
        begin

          stringgrid1.Cells[0, I] := Datamodule1.Uniquery2.FieldByName('nombre').AsString;
          stringgrid1.Cells[1, I] := Datamodule1.Uniquery2.FieldByName('descuento').AsString;
          stringgrid1.Cells[2, I] := Datamodule1.Uniquery2.FieldByName('precio_unico').AsString;
          stringgrid1.Cells[3, I] := Datamodule1.Uniquery2.FieldByName('precio_mayoreo').AsString;

          Datamodule1.Uniquery2.Next;
        end


      end;
end;

end.
