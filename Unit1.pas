unit Unit1;

interface

uses
  System.SysUtils, System.Classes, Data.DB, MemDS, DBAccess, Uni, UniProvider,
  MySQLUniProvider;

type
  TDataModule1 = class(TDataModule)
    MySQLUniProvider1: TMySQLUniProvider;
    UniConnection1: TUniConnection;
    UniQuery1: TUniQuery;
    UniQuery2: TUniQuery;
    UniQuery3: TUniQuery;
    UniQuery4: TUniQuery;
    UniQuery5: TUniQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DataModule1: TDataModule1;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

end.
