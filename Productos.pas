unit Productos;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.Controls.Presentation,
  FMX.ScrollBox, Unit1, FMX.Objects;

type
  TFrmProd = class(TFrame)
    GridProd: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    StringColumn5: TStringColumn;
    StringColumn6: TStringColumn;
    procedure iniciogrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}



procedure TFrmProd.iniciogrid;
var i: integer;
begin
   DataModule1.UniQuery5.Close;
   DataModule1.UniQuery5.SQL.Text := 'SELECT b.nombre AS "producto", SUM(a.cantidad) AS "t_cantidad", SUM(a.cantidad * a.costo_t) AS "t_total" FROM transaccion_detalles a JOIN productos b ON a.id_producto = b.id GROUP BY b.nombre ORDER BY t_cantidad DESC LIMIT 10';
   DataModule1.UniQuery5.Open;

   if DataModule1.UniQuery5.RecordCount >0 then
    begin

      for I := 0 to datamodule1.UniQuery5.RecordCount - 1 do
        begin
          GridProd.cells[0, I] := datamodule1.UniQuery5.FieldByName('producto').AsString;
          GridProd.cells[1, I] := datamodule1.UniQuery5.FieldByName('t_cantidad').AsString;
          GridProd.cells[2, I] := datamodule1.UniQuery5.FieldByName('t_total').AsString;
         



          datamodule1.UniQuery5.Next;
        end;

    end;

end;



end.
