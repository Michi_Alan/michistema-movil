unit celular_poryecto;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Controls.Presentation, FMX.Edit, FMX.Layouts, FMX.TabControl,
  FMX.ListBox, FMX.MultiView, usuario, System.ImageList, FMX.ImgList, Unit2,
  FrmInv, Clientes, Utilidad, Productos, FMX.Media, FireDAC.Stan.Intf,
  FireDAC.Comp.BatchMove, FireDAC.Comp.BatchMove.Text, {wcrypt2} System.Rtti,
  FMX.Grid.Style, FMX.Grid, FMX.ScrollBox;

  {function md5(const Input: String): String;}

type
  TForm7 = class(TForm)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    Layout1: TLayout;
    GridPanelLayout1: TGridPanelLayout;
    Rectangle1: TRectangle;
    Rectangle2: TRectangle;
    Layout2: TLayout;
    Edtusuario: TEdit;
    Edtpassword: TEdit;
    Text1: TText;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    Rectangle3: TRectangle;
    Layout3: TLayout;
    Rectangle4: TRectangle;
    ToolBar1: TToolBar;
    MultiView1: TMultiView;
    ListBox1: TListBox;
    Text2: TText;
    StyleBook1: TStyleBook;
    Text3: TText;
    Button2: TButton;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    ListBoxItem4: TListBoxItem;
    ListBoxItem5: TListBoxItem;
    ListBoxItem6: TListBoxItem;
    Frame31: TFrame3;
    ImageList1: TImageList;
    Frame21: TFrame2;
    FrmInv1: TFrmInv;
    ClientesDestacados1: TClientesDestacados;
    futilidad1: Tfutilidad;
    FrmProd1: TFrmProd;
    MediaPlayer1: TMediaPlayer;
    Layout4: TLayout;
    Rectangle5: TRectangle;
    Rectangle6: TRectangle;
    Rectangle7: TRectangle;
    Rectangle8: TRectangle;
    Text4: TText;
    Text5: TText;
    Text6: TText;
    Text7: TText;
    Text8: TText;
    Text9: TText;
    FDBatchMoveTextReader1: TFDBatchMoveTextReader;
    Text10: TText;
    Text11: TText;
    Text12: TText;
    Text13: TText;
    Text14: TText;
    gridin: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    Text15: TText;
    Text16: TText;
    Rectangle9: TRectangle;
    Text17: TText;
    Text18: TText;
    Text19: TText;
    Text20: TText;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Frame21ListBoxItem1Click(Sender: TObject);
    procedure ListBoxItem5Click(Sender: TObject);
    procedure Text3Click(Sender: TObject);
    procedure Frame31Button1Click(Sender: TObject);
    procedure Frame21Text1Click(Sender: TObject);
    procedure Frame21ListBoxItem4Click(Sender: TObject);
    procedure ListBoxItem6Click(Sender: TObject);
    procedure Frame21ListBoxItem6Click(Sender: TObject);
    procedure ListBoxItem2Click(Sender: TObject);
    procedure ListBoxItem1Click(Sender: TObject);
    procedure ListBoxItem3Click(Sender: TObject);
    procedure ListBoxItem4Click(Sender: TObject);
    procedure Text13Click(Sender: TObject);
    procedure Text14Click(Sender: TObject);
    procedure Text15Click(Sender: TObject);
    procedure Text16Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form7: TForm7;
  user_logged: string;
  contrasena: string;
  correo: string;
  descuento: string;
  nombre: string;
  nombre_pro: string;
  total: string;
  i: integer;
  total_can: string;

implementation

{$R *.fmx}

uses Unit1;
{
function md5(const Input: String): String;
var
  hCryptProvider: HCRYPTPROV;
  hHash: HCRYPTHASH;
  bHash: array[0..$7f] of Byte;
  dwHashBytes: Cardinal;
  pbContent: PByte;
  i: Integer;
begin
  dwHashBytes := 16;
  pbContent := Pointer(PChar(Input));
  Result := '';
  if CryptAcquireContext(@hCryptProvider, nil, nil, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT or CRYPT_MACHINE_KEYSET) then
  begin
    if CryptCreateHash(hCryptProvider, CALG_MD5, 0, 0, @hHash) then
    begin
      if CryptHashData(hHash, pbContent, Length(Input) * sizeof(Char), 0) then
      begin
        if CryptGetHashParam(hHash, HP_HASHVAL, @bHash[0], @dwHashBytes, 0) then
        begin
          for i := 0 to dwHashBytes - 1 do
          begin
            Result := Result + format('%.2x', [bHash[i]]);
          end;
        end;
      end;
      CryptDestroyHash(hHash);
    end;
    CryptReleaseContext(hCryptProvider, 0);
  end;
  Result := AnsiLowerCase(Result);
end;
     }

procedure TForm7.Button1Click(Sender: TObject);
begin

DataModule1.uniQuery1.Open;
DataModule1.uniQuery1.SQL.Text := 'select m.usuario, m.correo, m.pass '  +
                                'from usuarios_android m ' +
                                'where m.usuario = ''' + Edtusuario.text + '''';

 Datamodule1.UniQuery1.Open;
 if DataModule1.UniQuery1.RecordCount > 0 then
 begin
 contrasena := Datamodule1.UniQuery1.FieldByName('pass').AsString;

 if contrasena= (edtpassword.Text) then
 begin
   tabcontrol1.ActiveTab := Tabitem2;
   user_logged := Datamodule1.UniQuery1.FieldByName('usuario').AsString;
   correo := Datamodule1.UniQuery1.FieldByName('correo').AsString;
   Text2.Text := 'Bienvenido ';
   text3.Text := user_logged;
   frame31.Text3.Text := user_logged;
   frame31.Text5.Text := edtpassword.Text;
   frame31.Text7.Text := correo;
   layout4.Visible:= true;
 end
    else
    begin
      showmessage ('contraseņa o usuario incorrecta');
    end;


 end
 else
 showmessage('El usuario no puede quedar vacio');

  Datamodule1.uniquery2.close;
    Datamodule1.uniquery2.SQL.Text := 'select p.nombre, p.descuento, p.precio_unico, precio_mayoreo from productos p';

    Datamodule1.Uniquery2.Open;

    if Datamodule1.Uniquery2.RecordCount >0 then
    begin
    descuento := Datamodule1.UniQuery2.FieldByName('descuento').AsString;
    nombre := Datamodule1.UniQuery2.FieldByName('nombre').AsString;
    text8.Text := descuento;
    text9.Text := nombre;


      end;

      Datamodule1.uniquery5.close;
    Datamodule1.uniquery5.SQL.Text := 'SELECT b.nombre AS "producto", SUM(a.cantidad) AS "t_cantidad", SUM(a.cantidad * a.costo_t) AS "t_total" FROM transaccion_detalles a JOIN productos b ON a.id_producto = b.id GROUP BY b.nombre ORDER BY t_cantidad DESC LIMIT 10';

    Datamodule1.Uniquery5.Open;

    if Datamodule1.Uniquery5.RecordCount >0 then
    begin
    nombre_pro := Datamodule1.UniQuery5.FieldByName('producto').AsString;
    total := Datamodule1.UniQuery5.FieldByName('t_cantidad').AsString;
    total_can := Datamodule1.UniQuery5.FieldByName('t_total').AsString;
    text10.Text := nombre_pro;
    text11.Text := total;
    text12.Text := total_can;



      end;
      begin
   DataModule1.UniQuery3.Close;
   DataModule1.UniQuery3.SQL.Text := 'select a.direccion, p.nombre, p.precio_unico, i.localizacion_alm, i.existencia from inventario i join almacenes a on a.id = i.id_almacen join unidades_medidas um on um.id = i.id_medida join productos p on p.id = i.id_producto';
   DataModule1.UniQuery3.Open;

   if DataModule1.UniQuery3.RecordCount >0 then
    begin

      for I := 0 to datamodule1.UniQuery3.RecordCount - 1 do
        begin
          gridin.Cells[0, I] := datamodule1.UniQuery3.FieldByName('nombre').AsString;
          gridin.cells[1, I] := datamodule1.UniQuery3.FieldByName('existencia').AsString;

          datamodule1.UniQuery3.Next;
        end

    end


      end;


end;

procedure TForm7.FormShow(Sender: TObject);
begin
tabcontrol1.TabPosition := ttabposition.None;
end;

procedure TForm7.Frame21ListBoxItem1Click(Sender: TObject);
begin
frame21.Visible := false;
frame31.Visible := false;


  rectangle4.Visible := true;

end;

procedure TForm7.Frame21ListBoxItem4Click(Sender: TObject);
begin
frame21.Visible := true;
frame31.Visible := false;
end;

procedure TForm7.Frame21ListBoxItem6Click(Sender: TObject);
begin
FrmInv1.Visible := true;
frame31.Visible := false;
frame21.Visible := false;
end;

procedure TForm7.Frame21Text1Click(Sender: TObject);
begin
frame21.Visible := false;
frame31.Visible := true;
frame21.inicio;
end;

procedure TForm7.Frame31Button1Click(Sender: TObject);
begin
frame31.Visible := false;
frame21.Visible := true;

end;

procedure TForm7.ListBoxItem1Click(Sender: TObject);
begin
layout4.Visible := true;
text2.Visible := true;
  clientesdestacados1.Visible := false;
FrmInv1.visible := false;
frame21.Visible :=false;
frame31.Visible :=false;
futilidad1.Visible := false;
FrmProd1.Visible := false;
end;

procedure TForm7.ListBoxItem2Click(Sender: TObject);
begin
text2.Visible := false;
clientesdestacados1.Visible := true;
FrmInv1.visible := false;
frame21.Visible :=false;
frame31.Visible :=false;
futilidad1.Visible := false;
clientesdestacados1.inicio;
FrmProd1.Visible := false;
layout4.Visible := false;
end;

procedure TForm7.ListBoxItem3Click(Sender: TObject);
begin
text2.Visible := false;
clientesdestacados1.Visible := false;
FrmInv1.visible := false;
frame21.Visible :=false;
frame31.Visible :=false;
futilidad1.Visible := false;
FrmProd1.Visible := true;
frmprod1.iniciogrid;
layout4.Visible := false;
end;

procedure TForm7.ListBoxItem4Click(Sender: TObject);
begin
text2.Visible := false;
futilidad1.visible:= true;
clientesdestacados1.Visible := false;
FrmInv1.visible := false;
frame21.Visible :=false;
frame31.Visible :=false;
FrmProd1.Visible := false;
layout4.Visible := false;
end;

procedure TForm7.ListBoxItem5Click(Sender: TObject);
begin
text2.Visible := false;
   frame21.Visible := true;
   frame21.inicio;
 FrmInv1.visible := false;
 clientesdestacados1.Visible := false;
frame31.Visible :=false;
futilidad1.Visible := false;
FrmProd1.Visible := false;
layout4.Visible := false;


end;

procedure TForm7.ListBoxItem6Click(Sender: TObject);
begin
text2.Visible := false;
FrmInv1.visible := true;
FrmInv1.iniciogrid;
frame21.Visible:= false;
clientesdestacados1.Visible := false;
frame31.Visible :=false;
futilidad1.Visible := false;
FrmProd1.Visible := false;
layout4.Visible := false;
end;

procedure TForm7.Text13Click(Sender: TObject);
begin
text2.Visible := false;
   frame21.Visible := true;
   frame21.inicio;
 FrmInv1.visible := false;
 clientesdestacados1.Visible := false;
frame31.Visible :=false;
futilidad1.Visible := false;
FrmProd1.Visible := false;
layout4.Visible := false;


end;

procedure TForm7.Text14Click(Sender: TObject);
begin
text2.Visible := false;
clientesdestacados1.Visible := false;
FrmInv1.visible := false;
frame21.Visible :=false;
frame31.Visible :=false;
futilidad1.Visible := false;
FrmProd1.Visible := true;
frmprod1.iniciogrid;
layout4.Visible := false;
end;

procedure TForm7.Text15Click(Sender: TObject);
begin
text2.Visible := false;
FrmInv1.visible := true;
FrmInv1.iniciogrid;
frame21.Visible:= false;
clientesdestacados1.Visible := false;
frame31.Visible :=false;
futilidad1.Visible := false;
FrmProd1.Visible := false;
layout4.Visible := false;
end;

procedure TForm7.Text16Click(Sender: TObject);
begin
text2.Visible := false;
futilidad1.visible:= true;
clientesdestacados1.Visible := false;
FrmInv1.visible := false;
frame21.Visible :=false;
frame31.Visible :=false;
FrmProd1.Visible := false;
layout4.Visible := false;
end;

procedure TForm7.Text3Click(Sender: TObject);
begin
text2.Visible := false;
FrmInv1.visible := false;
frame21.Visible:= false;
clientesdestacados1.Visible := false;
futilidad1.Visible := false;
frame31.Visible := true;
FrmProd1.Visible := false;
layout4.Visible := false;
end;

end.
