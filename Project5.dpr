program Project5;

uses
  System.StartUpCopy,
  FMX.Forms,
  celular_poryecto in 'celular_poryecto.pas' {Form7},
  Unit1 in 'Unit1.pas' {DataModule1: TDataModule},
  usuario in 'usuario.pas' {Frame3: TFrame},
  Unit2 in 'Unit2.pas' {Frame2: TFrame},
  FrmInv in 'FrmInv.pas' {FrmInv: TFrame},
  Clientes in 'Clientes.pas' {ClientesDestacados: TFrame},
  Utilidad in 'Utilidad.pas' {futilidad: TFrame},
  Productos in 'Productos.pas' {FrmProd: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.FormFactor.Orientations := [TFormOrientation.Landscape];
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TDataModule1, DataModule1);
  Application.Run;
end.
