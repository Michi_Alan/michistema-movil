unit Clientes;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView, System.Rtti, FMX.Grid.Style, FMX.Objects, FMX.Grid,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Layouts, unit1;

type
  TClientesDestacados = class(TFrame)
  Layout1: TLayout;
    Rectangle1: TRectangle;
    stringclte: TStringGrid;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    procedure inicio;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

var
I: integer;
producto: string;

{$R *.fmx}
procedure tclientesdestacados.inicio;
begin

  Datamodule1.uniquery4.close;
    Datamodule1.uniquery4.SQL.Text := 'select c.id, c.nombre, c.rfc, c.telefono from clientes c';

    Datamodule1.Uniquery4.Open;

    if Datamodule1.Uniquery4.RecordCount >0 then
    begin

      for I := 0 to Datamodule1.Uniquery4.RecordCount - 1 do
        begin

        stringclte.cells[0, I] := datamodule1.UniQuery4.FieldByName('id').AsString;
          stringclte.cells[1, I] := datamodule1.UniQuery4.FieldByName('nombre').AsString;
          stringclte.cells[2, I] := datamodule1.UniQuery4.FieldByName('rfc').AsString;
          stringclte.cells[3, I] := datamodule1.UniQuery4.FieldByName('telefono').AsString;

          Datamodule1.Uniquery4.Next;
        end


      end;
end;
end.
